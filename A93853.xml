<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A93853">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Stephen Langthon, Arch-bishop of Canterbury, who dyed in the reign of Henry III. Ann Dom. 1228. was the first that distinguished the chapters of the Bible into that order and number as we now use them. ....</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A93853 of text R205309 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason E480_4). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A93853</idno>
    <idno type="STC">Wing S5412</idno>
    <idno type="STC">Thomason E480_4</idno>
    <idno type="STC">ESTC R205309</idno>
    <idno type="EEBO-CITATION">99864721</idno>
    <idno type="PROQUEST">99864721</idno>
    <idno type="VID">116953</idno>
    <idno type="PROQUESTGOID">2240920046</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A93853)</note>
    <note>Transcribed from: (Early English Books Online ; image set 116953)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 77:E480[4])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Stephen Langthon, Arch-bishop of Canterbury, who dyed in the reign of Henry III. Ann Dom. 1228. was the first that distinguished the chapters of the Bible into that order and number as we now use them. ....</title>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>for Samuel Mearne,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1655]</date>
     </publicationStmt>
     <notesStmt>
      <note>Imprint from Wing.</note>
      <note>A list of the twelve "compilers of the English Common-Prayer Book".</note>
      <note>Annotation on Thomason copy: "Febr. 28. 1654".</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Langton, Stephen, d. 1228 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A93853</ep:tcp>
    <ep:estc> R205309</ep:estc>
    <ep:stc> (Thomason E480_4). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>Stephen Langthon, Arch-bishop of Canterbury, who dyed in the reign of Henry III. Ann Dom. 1228. was the first that distinguished the chapter</ep:title>
    <ep:author>anon.</ep:author>
    <ep:publicationYear>1655</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>2</ep:pageCount>
    <ep:wordCount>154</ep:wordCount>
    <ep:defectiveTokenCount>1</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>65</ep:defectRate>
    <ep:finalGrade>D</ep:finalGrade>
    <ep:defectRangePerGrade> The  rate of 65 defects per 10,000 words puts this text in the D category of texts with between 35 and 100 defects per 10,000 words.</ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-06</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-07</date><label>Robyn Anspach</label>
        Sampled and proofread
      </change>
   <change><date>2007-07</date><label>Robyn Anspach</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A93853-e10">
  <body xml:id="A93853-e20">
   <pb facs="tcp:116953:1" xml:id="A93853-001-a"/>
   <pb facs="tcp:116953:1" rend="simple:additions" xml:id="A93853-001-b"/>
   <div type="document" xml:id="A93853-e30">
    <p xml:id="A93853-e40">
     <w lemma="Stephen" pos="nn1" xml:id="A93853-001-b-0010">Stephen</w>
     <w lemma="Langthon" pos="nn1" xml:id="A93853-001-b-0020">Langthon</w>
     <pc xml:id="A93853-001-b-0030">,</pc>
     <hi xml:id="A93853-e50">
      <w lemma="archbishop" pos="n1" reg="Archbishop" xml:id="A93853-001-b-0040">Arch-bishop</w>
      <w lemma="of" pos="acp" xml:id="A93853-001-b-0050">of</w>
     </hi>
     <w lemma="Canterbury" pos="nn1" xml:id="A93853-001-b-0060">Canterbury</w>
     <pc xml:id="A93853-001-b-0070">,</pc>
     <hi xml:id="A93853-e60">
      <w lemma="who" pos="crq" xml:id="A93853-001-b-0080">who</w>
      <w lemma="die" pos="vvd" reg="died" xml:id="A93853-001-b-0090">dyed</w>
      <w lemma="in" pos="acp" xml:id="A93853-001-b-0100">in</w>
      <w lemma="the" pos="d" xml:id="A93853-001-b-0110">the</w>
      <w lemma="reign" pos="n1" xml:id="A93853-001-b-0120">Reign</w>
      <w lemma="of" pos="acp" xml:id="A93853-001-b-0130">of</w>
     </hi>
     <w lemma="Henry" pos="nn1" xml:id="A93853-001-b-0140">Henry</w>
     <hi xml:id="A93853-e70">
      <w lemma="iii" pos="crd" xml:id="A93853-001-b-0150">III.</w>
     </hi>
     <w lemma="ann" pos="ab" xml:id="A93853-001-b-0160">Ann</w>
     <w lemma="dom." pos="ab" xml:id="A93853-001-b-0170">Dom.</w>
     <hi xml:id="A93853-e80">
      <w lemma="1228." pos="crd" xml:id="A93853-001-b-0190">1228.</w>
      <w lemma="be" pos="vvd" xml:id="A93853-001-b-0200">was</w>
      <w lemma="the" pos="d" xml:id="A93853-001-b-0210">the</w>
      <w lemma="first" pos="ord" xml:id="A93853-001-b-0220">first</w>
      <w lemma="that" pos="cs" xml:id="A93853-001-b-0230">that</w>
      <w lemma="distinguish" pos="vvn" xml:id="A93853-001-b-0240">distinguished</w>
      <w lemma="the" pos="d" xml:id="A93853-001-b-0250">the</w>
      <w lemma="chapter" pos="n2" xml:id="A93853-001-b-0260">Chapters</w>
      <w lemma="of" pos="acp" xml:id="A93853-001-b-0270">of</w>
      <w lemma="the" pos="d" xml:id="A93853-001-b-0280">the</w>
      <w lemma="bible" pos="n1" xml:id="A93853-001-b-0290">BIBLE</w>
      <w lemma="into" pos="acp" xml:id="A93853-001-b-0300">into</w>
      <w lemma="that" pos="d" xml:id="A93853-001-b-0310">that</w>
      <w lemma="order" pos="n1" xml:id="A93853-001-b-0320">order</w>
      <w lemma="and" pos="cc" xml:id="A93853-001-b-0330">and</w>
      <w lemma="number" pos="n1" xml:id="A93853-001-b-0340">number</w>
      <w lemma="as" pos="acp" xml:id="A93853-001-b-0350">as</w>
      <w lemma="we" pos="pns" xml:id="A93853-001-b-0360">we</w>
      <w lemma="now" pos="av" xml:id="A93853-001-b-0370">now</w>
      <w lemma="use" pos="vvb" xml:id="A93853-001-b-0380">use</w>
      <w lemma="they" pos="pno" xml:id="A93853-001-b-0390">them</w>
      <pc unit="sentence" xml:id="A93853-001-b-0400">.</pc>
     </hi>
    </p>
    <p xml:id="A93853-e90">
     <hi xml:id="A93853-e100">
      <w lemma="the" pos="d" xml:id="A93853-001-b-0410">The</w>
      <w lemma="compiler" pos="n2" xml:id="A93853-001-b-0420">Compilers</w>
      <w lemma="of" pos="acp" xml:id="A93853-001-b-0430">of</w>
      <w lemma="the" pos="d" xml:id="A93853-001-b-0440">the</w>
     </hi>
     <w lemma="english" pos="jnn" xml:id="A93853-001-b-0450">English</w>
     <w lemma="commonprayer" pos="n1" reg="commonprayer" xml:id="A93853-001-b-0460">COMMON-PRAYER</w>
     <hi xml:id="A93853-e110">
      <w lemma="book" pos="n1" xml:id="A93853-001-b-0470">Book</w>
      <pc join="right" xml:id="A93853-001-b-0480">(</pc>
      <w lemma="as" pos="acp" xml:id="A93853-001-b-0490">as</w>
      <w lemma="now" pos="av" xml:id="A93853-001-b-0500">now</w>
      <w lemma="it" pos="pn" xml:id="A93853-001-b-0510">it</w>
      <w lemma="be" pos="vvz" xml:id="A93853-001-b-0520">is</w>
      <pc xml:id="A93853-001-b-0530">)</pc>
      <w lemma="be" pos="vvd" xml:id="A93853-001-b-0540">were</w>
      <pc xml:id="A93853-001-b-0550">,</pc>
     </hi>
     <list xml:id="A93853-e120">
      <item xml:id="A93853-e130">
       <hi xml:id="A93853-e140">
        <w lemma="dr." pos="ab" xml:id="A93853-001-b-0560">Dr.</w>
       </hi>
       <w lemma="Cranmer" pos="nn1" xml:id="A93853-001-b-0580">Cranmer</w>
       <hi xml:id="A93853-e160">
        <w lemma="archbishop" pos="n1" reg="Archbishop" xml:id="A93853-001-b-0590">Arch-bishop</w>
        <w lemma="of" pos="acp" xml:id="A93853-001-b-0600">of</w>
       </hi>
       <w lemma="Canterbury" pos="nn1" xml:id="A93853-001-b-0610">Canterbury</w>
       <pc xml:id="A93853-001-b-0620">,</pc>
       <hi xml:id="A93853-e170">
        <w lemma="martyr" pos="n1" xml:id="A93853-001-b-0630">Martyr</w>
        <pc unit="sentence" xml:id="A93853-001-b-0640">.</pc>
       </hi>
      </item>
      <item xml:id="A93853-e180">
       <hi xml:id="A93853-e190">
        <w lemma="dr." pos="ab" xml:id="A93853-001-b-0650">Dr.</w>
       </hi>
       <w lemma="Goodrick" pos="nn1" xml:id="A93853-001-b-0670">Goodrick</w>
       <hi xml:id="A93853-e210">
        <w lemma="bishop" pos="n1" xml:id="A93853-001-b-0680">Bishop</w>
        <w lemma="of" pos="acp" xml:id="A93853-001-b-0690">of</w>
       </hi>
       <w lemma="Elie" pos="nn1" xml:id="A93853-001-b-0700">Ely</w>
       <pc xml:id="A93853-001-b-0710">,</pc>
      </item>
      <item xml:id="A93853-e220">
       <hi xml:id="A93853-e230">
        <w lemma="dr." pos="ab" xml:id="A93853-001-b-0720">Dr.</w>
       </hi>
       <w lemma="Skip" pos="nn1" xml:id="A93853-001-b-0740">Skip</w>
       <hi xml:id="A93853-e250">
        <w lemma="bishop" pos="n1" xml:id="A93853-001-b-0750">Bishop</w>
        <w lemma="of" pos="acp" xml:id="A93853-001-b-0760">of</w>
       </hi>
       <w lemma="Hereford" pos="nn1" xml:id="A93853-001-b-0770">Hereford</w>
       <pc xml:id="A93853-001-b-0780">,</pc>
      </item>
      <item xml:id="A93853-e260">
       <hi xml:id="A93853-e270">
        <w lemma="dr." pos="ab" xml:id="A93853-001-b-0790">Dr.</w>
       </hi>
       <w lemma="thirlby" pos="nn1" xml:id="A93853-001-b-0810">Thirlby</w>
       <hi xml:id="A93853-e290">
        <w lemma="bishop" pos="n1" xml:id="A93853-001-b-0820">Bishop</w>
        <w lemma="of" pos="acp" xml:id="A93853-001-b-0830">of</w>
       </hi>
       <w lemma="Westminster" pos="nn1" xml:id="A93853-001-b-0840">Westminster</w>
       <pc xml:id="A93853-001-b-0850">,</pc>
      </item>
      <item xml:id="A93853-e300">
       <hi xml:id="A93853-e310">
        <w lemma="dr." pos="ab" xml:id="A93853-001-b-0860">Dr.</w>
       </hi>
       <w lemma="day" pos="nn1" xml:id="A93853-001-b-0880">Day</w>
       <hi xml:id="A93853-e330">
        <w lemma="bishop" pos="n1" xml:id="A93853-001-b-0890">Bishop</w>
        <w lemma="of" pos="acp" xml:id="A93853-001-b-0900">of</w>
       </hi>
       <w lemma="Chichester" pos="nn1" xml:id="A93853-001-b-0910">Chichester</w>
       <pc xml:id="A93853-001-b-0920">,</pc>
      </item>
      <item xml:id="A93853-e340">
       <hi xml:id="A93853-e350">
        <w lemma="dr." pos="ab" xml:id="A93853-001-b-0930">Dr.</w>
       </hi>
       <w lemma="holbeck" pos="nn1" xml:id="A93853-001-b-0950">Holbeck</w>
       <hi xml:id="A93853-e370">
        <w lemma="bishop" pos="n1" xml:id="A93853-001-b-0960">Bishop</w>
        <w lemma="of" pos="acp" xml:id="A93853-001-b-0970">of</w>
       </hi>
       <w lemma="Lincoln" pos="nn1" xml:id="A93853-001-b-0980">Lincoln</w>
       <pc xml:id="A93853-001-b-0990">,</pc>
      </item>
      <item xml:id="A93853-e380">
       <hi xml:id="A93853-e390">
        <w lemma="dr." pos="ab" xml:id="A93853-001-b-1000">Dr.</w>
       </hi>
       <w lemma="Ridley" pos="nn1" xml:id="A93853-001-b-1020">Ridley</w>
       <hi xml:id="A93853-e410">
        <w lemma="bishop" pos="n1" xml:id="A93853-001-b-1030">Bishop</w>
        <w lemma="of" pos="acp" xml:id="A93853-001-b-1040">of</w>
       </hi>
       <w lemma="Rochester" pos="nn1" xml:id="A93853-001-b-1050">Rochester</w>
       <pc xml:id="A93853-001-b-1060">,</pc>
       <hi xml:id="A93853-e420">
        <w lemma="and" pos="cc" xml:id="A93853-001-b-1070">and</w>
        <w lemma="after" pos="acp" xml:id="A93853-001-b-1080">after</w>
        <w lemma="bishop" pos="n1" xml:id="A93853-001-b-1090">Bishop</w>
        <w lemma="of" pos="acp" xml:id="A93853-001-b-1100">of</w>
       </hi>
       <w lemma="London" pos="nn1" xml:id="A93853-001-b-1110">London</w>
       <pc xml:id="A93853-001-b-1120">,</pc>
       <hi xml:id="A93853-e430">
        <w lemma="martyr" pos="n1" xml:id="A93853-001-b-1130">Martyr</w>
        <pc unit="sentence" xml:id="A93853-001-b-1140">.</pc>
       </hi>
      </item>
      <item xml:id="A93853-e440">
       <hi xml:id="A93853-e450">
        <w lemma="dr." pos="ab" xml:id="A93853-001-b-1150">Dr.</w>
       </hi>
       <w lemma="Cox" pos="nn1" xml:id="A93853-001-b-1170">Cox</w>
       <hi xml:id="A93853-e470">
        <w lemma="king" pos="n1" xml:id="A93853-001-b-1180">King</w>
       </hi>
       <w lemma="Edward" pos="nng1" rend="hi-apo-plain" xml:id="A93853-001-b-1190">Edward's</w>
       <hi xml:id="A93853-e480">
        <w lemma="almoner" pos="n1" xml:id="A93853-001-b-1210">Almoner</w>
        <pc xml:id="A93853-001-b-1220">,</pc>
       </hi>
      </item>
      <item xml:id="A93853-e490">
       <hi xml:id="A93853-e500">
        <w lemma="dr." pos="ab" xml:id="A93853-001-b-1230">Dr.</w>
       </hi>
       <w lemma="Taylor" pos="nn1" xml:id="A93853-001-b-1250">Taylor</w>
       <w lemma="dean" pos="n1" xml:id="A93853-001-b-1260">Dean</w>
       <w lemma="of" pos="acp" xml:id="A93853-001-b-1270">of</w>
       <w lemma="Lincoln" pos="nn1" xml:id="A93853-001-b-1280">Lincoln</w>
       <pc xml:id="A93853-001-b-1290">,</pc>
      </item>
      <item xml:id="A93853-e520">
       <hi xml:id="A93853-e530">
        <w lemma="dr." pos="ab" xml:id="A93853-001-b-1300">Dr.</w>
       </hi>
       <w lemma="heynes" pos="nn1" xml:id="A93853-001-b-1320">Heynes</w>
       <w lemma="dean" pos="n1" xml:id="A93853-001-b-1330">Dean</w>
       <w lemma="of" pos="acp" xml:id="A93853-001-b-1340">of</w>
       <w lemma="Exeter" pos="nn1" reg="Exeter" xml:id="A93853-001-b-1350">Exceter</w>
       <pc xml:id="A93853-001-b-1360">,</pc>
      </item>
      <item xml:id="A93853-e550">
       <hi xml:id="A93853-e560">
        <w lemma="dr." pos="ab" xml:id="A93853-001-b-1370">Dr.</w>
       </hi>
       <w lemma="Redman" pos="nn1" xml:id="A93853-001-b-1390">Redman</w>
       <w lemma="dean" pos="n1" xml:id="A93853-001-b-1400">Dean</w>
       <w lemma="of" pos="acp" xml:id="A93853-001-b-1410">of</w>
       <w lemma="Westminster" pos="nn1" xml:id="A93853-001-b-1420">Westminster</w>
       <pc xml:id="A93853-001-b-1430">,</pc>
      </item>
      <item xml:id="A93853-e580">
       <hi xml:id="A93853-e590">
        <w lemma="mr." pos="ab" xml:id="A93853-001-b-1440">Mr.</w>
       </hi>
       <w lemma="Robinson" pos="nn1" xml:id="A93853-001-b-1460">Robinson</w>
       <hi xml:id="A93853-e610">
        <w lemma="archdeacon" pos="n1" reg="archdeacon" xml:id="A93853-001-b-1470">Arch-deacon</w>
        <w lemma="of" pos="acp" xml:id="A93853-001-b-1480">of</w>
       </hi>
       <w lemma="Leicester" pos="nn1" xml:id="A93853-001-b-1490">Leicester</w>
       <pc unit="sentence" xml:id="A93853-001-b-1500">.</pc>
      </item>
     </list>
    </p>
   </div>
  </body>
  <back xml:id="A93853-e620">
   <div type="colophon" xml:id="A93853-e630">
    <p xml:id="A93853-e640">
     <w lemma="in" pos="acp" xml:id="A93853-001-b-1510">In</w>
     <hi xml:id="A93853-e650">
      <w lemma="May" pos="nn1" xml:id="A93853-001-b-1520">May</w>
      <pc xml:id="A93853-001-b-1530">,</pc>
      <w lemma="anno" pos="fla" xml:id="A93853-001-b-1540">Anno</w>
      <w lemma="dom." pos="ab" xml:id="A93853-001-b-1550">Dom.</w>
     </hi>
     <w lemma="1549" pos="crd" xml:id="A93853-001-b-1560">1549</w>
     <pc xml:id="A93853-001-b-1570">▪</pc>
     <w lemma="and" pos="cc" xml:id="A93853-001-b-1580">and</w>
     <w lemma="the" pos="d" xml:id="A93853-001-b-1590">the</w>
     <w lemma="three" pos="ord" xml:id="A93853-001-b-1600">third</w>
     <w lemma="year" pos="n1" reg="year" xml:id="A93853-001-b-1610">yeer</w>
     <w lemma="of" pos="acp" xml:id="A93853-001-b-1620">of</w>
     <w lemma="the" pos="d" xml:id="A93853-001-b-1630">the</w>
     <w lemma="reign" pos="n1" xml:id="A93853-001-b-1640">Reign</w>
     <w lemma="of" pos="acp" xml:id="A93853-001-b-1650">of</w>
     <hi xml:id="A93853-e660">
      <w lemma="Edward" pos="nn1" xml:id="A93853-001-b-1660">Edward</w>
     </hi>
     <w lemma="vi" pos="crd" xml:id="A93853-001-b-1670">VI</w>
     <pc unit="sentence" xml:id="A93853-001-b-1680">.</pc>
    </p>
   </div>
  </back>
 </text>
</TEI>
